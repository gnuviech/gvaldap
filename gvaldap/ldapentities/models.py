"""
This module defines models for LDAP entities.

The models are based on :py:class:`ldapmodels.Model` from `django-ldapdb`_.

.. _django-ldapdb: https://github.com/jlaine/django-ldapdb#readme

"""
from __future__ import unicode_literals

from django.conf import settings
from ldapdb.models.fields import (
    CharField,
    IntegerField,
    ListField,
)
import ldapdb.models as ldapmodels

from passlib.hash import ldap_salted_sha1


class LdapGroup(ldapmodels.Model):
    """
    Class for representing an LDAP group entity with objectClass `posixGroup`.

    .. seealso:: :rfc:`2307#section-4`

    .. py:attribute:: base_dn

       a string containing the LDAP base distinguished name

    .. py:attribute:: members

       contains the list of `memberUid` attributes

    """
    # LDAP meta-data
    base_dn = settings.GROUP_BASE_DN
    #: list of object classes
    object_classes = ['posixGroup']

    # posixGroup attributes
    #: group id (`gidNumber`)
    gid = IntegerField(db_column='gidNumber', unique=True)
    #: group name (`cn`)
    name = CharField(db_column='cn', max_length=200, primary_key=True)
    #: group description (`description`)
    description = CharField(db_column='description')
    members = ListField(db_column='memberUid', blank=True)

    def __str__(self):
        """
        Get a string representation of this LDAP group.
        """
        return self.name


class LdapUser(ldapmodels.Model):
    """
    Class for representing an LDAP user entity with objectClasses `account` and
    `posixAccount`.

    .. seealso:: :rfc:`2307#section-4`, :rfc:`4524#section-3.1`

    .. py:attribute:: base_dn

       a string containing the LDAP base distinguished name

    """
    base_dn = settings.USER_BASE_DN
    #: list of object classes
    object_classes = ['account', 'posixAccount']

    # posixAccount
    #: user id (`uidNumber`)
    uid = IntegerField(db_column='uidNumber', unique=True)
    #: group id (`gidNumber`) of the user's primary group
    group = IntegerField(db_column='gidNumber')
    #: GECOS field (`gecos`)
    gecos = CharField(db_column='gecos')
    #: home directory (`homeDirectory`)
    home_directory = CharField(db_column='homeDirectory')
    #: login shell (`loginShell`)
    login_shell = CharField(db_column='loginShell', default='/bin/bash')
    #: user name (`uid`)
    username = CharField(db_column='uid', primary_key=True)
    #: password (`userPassword`) in an LDAP compatible format
    password = CharField(db_column='userPassword')
    #: common name (`cn`)
    common_name = CharField(db_column='cn')

    def __str__(self):
        """
        Get a string representation of this LDAP user.
        """
        return self.username

    def set_password(self, password):
        """
        Sets the encrypted password of the user from the given clear text
        password.

        :param str password: the clear text password

        """
        self.password = ldap_salted_sha1.encrypt(password)
