"""
This app takes care of managing LDAP entities, at the moment these are:

* LDAP groups (:py:class:`ldapentities.models.LdapGroup`).
* LDAP users (:py:class:`ldapentities.models.LdapUser`)

"""
