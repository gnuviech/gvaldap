"""
Admin classes for easy `django admin`_ based administration of LDAP entities.

.. _django admin: https://docs.djangoproject.com/en/dev/ref/contrib/admin/

"""

from django.contrib import admin

from ldapentities.models import (
    LdapGroup,
    LdapUser,
)


class LdapGroupAdmin(admin.ModelAdmin):
    """
    Admin class for :py:class:`LDAP group <ldapentities.models.LdapGroup>`
    entities.

    """
    exclude = ['dn']
    list_display = ['name', 'gid']
    search_fields = ['name']


class LdapUserAdmin(admin.ModelAdmin):
    """
    Admin class for :py:class:`LDAP user <ldapentities.models.LdapUser>`
    entities.

    """
    exclude = ['dn', 'password']
    list_display = ['username', 'uid']
    search_fields = ['username']


admin.site.register(LdapGroup, LdapGroupAdmin)
admin.site.register(LdapUser, LdapUserAdmin)
