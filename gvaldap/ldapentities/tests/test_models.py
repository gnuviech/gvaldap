"""
This model provides tests for :py:mod:`ldapentities.models`.

"""
from __future__ import absolute_import, unicode_literals
from django.test import TestCase

from passlib.hash import ldap_salted_sha1

from ldapentities.models import LdapGroup, LdapUser


class LdapGroupTest(TestCase):

    def test___str__(self):
        ldapgroup = LdapGroup(
            gid=5000, name='test', description='test group')
        self.assertEqual(str(ldapgroup), 'test')


class LdapUserTest(TestCase):

    def test___str__(self):
        ldapuser = LdapUser(
            uid=5000, group=5000, gecos="a test user",
            home_directory='/home/test', login_shell='/bin/bash',
            username='test', password='test', common_name='Test')
        self.assertEqual(str(ldapuser), 'test')

    def test_set_password(self):
        ldapuser = LdapUser(
            uid=5000, group=5000, gecos="a test user",
            home_directory='/home/test', login_shell='/bin/bash',
            username='test', password='test', common_name='Test')
        self.assertEqual(ldapuser.password, 'test')
        ldapuser.set_password('test2')
        self.assertTrue(ldap_salted_sha1.verify('test2', ldapuser.password))
