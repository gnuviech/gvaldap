"""
This module provides tests for :py:mod:`ldapentities.admin`.

"""
from __future__ import absolute_import

import volatildap
from django.conf import settings
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

User = get_user_model()

TEST_USER = 'admin'
TEST_EMAIL = 'admin@example.org'
TEST_PASSWORD = 'secret'

admin = (settings.DATABASES['ldap']['USER'], {
    'objectClass': ['person'],
    'userPassword': [
        settings.DATABASES['ldap'][
            'PASSWORD']],
    'sn': 'Admin',
})

groups = (settings.GROUP_BASE_DN, {
    'objectClass': ['top', 'organizationalUnit'], 'ou': ['groups']})
users = (
    settings.USER_BASE_DN, {
        'objectClass': ['top', 'organizationalUnit'], 'ou': ['users']})


class LdapUserAdminTest(TestCase):
    databases = ["default", "ldap"]
    directory = dict([admin, groups, users])

    @classmethod
    def setUpClass(cls):
        super(LdapUserAdminTest, cls).setUpClass()
        cls.ldap_server = volatildap.LdapServer(
            initial_data=cls.directory,
            schemas=['core.schema', 'cosine.schema', 'inetorgperson.schema',
                     'nis.schema'],
        )
        settings.DATABASES['ldap']['USER'] = cls.ldap_server.rootdn
        settings.DATABASES['ldap']['PASSWORD'] = cls.ldap_server.rootpw
        settings.DATABASES['ldap']['NAME'] = cls.ldap_server.uri

    @classmethod
    def tearDownClass(cls):
        cls.ldap_server.stop()
        super(LdapUserAdminTest, cls).tearDownClass()

    def setUp(self):
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        self.ldap_server.start()

    def test_can_administer_groups(self):
        response = self.client.get(reverse(
            'admin:ldapentities_ldapgroup_changelist'))
        self.assertEqual(response.status_code, 200)

    def test_can_administer_users(self):
        response = self.client.get(reverse(
            'admin:ldapentities_ldapuser_changelist'))
        self.assertEqual(response.status_code, 200)
