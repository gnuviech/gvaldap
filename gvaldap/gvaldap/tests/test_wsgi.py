"""
This module provides tests for :py:mod:`gvaldap.wsgi`.

"""
from unittest import TestCase


class WSGITest(TestCase):

    def test_wsgi_application(self):
        from gvaldap import wsgi
        self.assertIsNotNone(wsgi.application)
