"""
This is the gvaldap project module.
"""
__version__ = "0.7.0"

from ldaptasks.celery import app as celery_app

__all__ = ("celery_app",)
