"""
This module defines the main URLConf for gvaldap.

"""

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:  # pragma: no cover
    import debug_toolbar

    urlpatterns = (
            [
                url(r'^__debug__/', include(debug_toolbar.urls)),
            ]
            + staticfiles_urlpatterns()
            + urlpatterns
    )
