"""
This module defines the Celery_ app for gvaldap.

.. _Celery: http://www.celeryproject.org/

"""
import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gvaldap.settings")

app = Celery("ldaptasks")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()
