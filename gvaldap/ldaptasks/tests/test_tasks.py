"""
This module provides tests for :py:mod:`ldaptasks.tasks`.

"""
import volatildap
from celery.exceptions import Reject

from django.conf import settings
from django.test import TestCase

from ldapentities.models import LdapUser
from ldaptasks.tasks import (
    add_ldap_user_to_group,
    create_ldap_group,
    create_ldap_user,
    delete_ldap_group,
    delete_ldap_group_if_empty,
    delete_ldap_user,
    delete_ldap_user_chained,
    remove_ldap_user_from_group,
    set_ldap_user_password,
)


class LdapTaskTestCase(TestCase):
    databases = ["default", "ldap"]

    directory = {
        settings.DATABASES['ldap']['USER']: {
            'objectClass': ['person'],
            'userPassword': [settings.DATABASES['ldap']['PASSWORD']],
            'sn': 'Admin',
        },
        settings.GROUP_BASE_DN: {
            'objectClass': ['top', 'organizationalUnit'],
            'ou': ['groups']
        },
        settings.USER_BASE_DN: {
            'objectClass': ['top', 'organizationalUnit'],
            'ou': ['users']
        },
        'cn=existing,' + settings.GROUP_BASE_DN: {
            'objectClass': ['posixGroup'],
            'gidNumber': ['4711'],
            'cn': ['existing'],
            'description': ['existing test group'],
            'memberUid': ['existing'],
        },
        'uid=existing,' + settings.USER_BASE_DN: {
            'objectClass': ['account', 'posixAccount'],
            'uidNumber': ['815'],
            'gidNumber': ['4711'],
            'gecos': ['existing test user'],
            'homeDirectory': ['/home/existing'],
            'loginShell': ['/bin/bash'],
            'uid': ['existing'],
            'userPassword': ['secret'],
            'cn': ['existing']
        }
    }

    @classmethod
    def setUpClass(cls):
        super(LdapTaskTestCase, cls).setUpClass()
        cls.ldap_server = volatildap.LdapServer(
            initial_data=cls.directory,
            schemas=['core.schema', 'cosine.schema', 'inetorgperson.schema',
                     'nis.schema'],
        )
        settings.DATABASES['ldap']['USER'] = cls.ldap_server.rootdn
        settings.DATABASES['ldap']['PASSWORD'] = cls.ldap_server.rootpw
        settings.DATABASES['ldap']['NAME'] = cls.ldap_server.uri

    @classmethod
    def tearDownClass(cls):
        cls.ldap_server.stop()
        super(LdapTaskTestCase, cls).tearDownClass()

    def setUp(self):
        self.ldap_server.start()

    def test_create_ldap_group(self):
        result = create_ldap_group('test', 5000, 'test group')
        self.assertEqual({
            'groupname': 'test', 'gid': 5000, 'description': 'test group',
            'group_dn': 'cn=test,%s' % settings.GROUP_BASE_DN
        }, result)

    def test_create_ldap_group_existing(self):
        result = create_ldap_group('existing', 4711, 'existing test group')
        self.assertEqual({
            'groupname': 'existing', 'gid': 4711,
            'description': 'existing test group',
            'group_dn': 'cn=existing,%s' % settings.GROUP_BASE_DN
        }, result)

    def test_create_ldap_group_existing_modify(self):
        result = create_ldap_group(
            'existing', 4711, 'change existing test group')
        self.assertEqual({
            'groupname': 'existing', 'gid': 4711,
            'description': 'change existing test group',
            'group_dn': 'cn=existing,%s' % settings.GROUP_BASE_DN
        }, result)

    def test_create_ldap_user(self):
        result = create_ldap_user(
            'test', 5000, 4711, 'Test User', '/home/test', '/bin/bash',
            'secret')
        self.assertEqual({
            'username': 'test', 'uid': 5000, 'gid': 4711, 'gecos': 'Test User',
            'homedir': '/home/test', 'shell': '/bin/bash',
            'user_dn': 'uid=test,%s' % settings.USER_BASE_DN
        }, result)

    def test_create_ldap_user_invalid_group(self):
        with self.assertRaises(Reject):
            create_ldap_user(
                'test', 5000, 5000, 'Test User', '/home/test', '/bin/bash',
                'secret')

    def test_create_ldap_user_no_password(self):
        result = create_ldap_user(
            'test', 5000, 4711, 'Test User', '/home/test', '/bin/bash',
            None)
        self.assertEqual({
            'username': 'test', 'uid': 5000, 'gid': 4711, 'gecos': 'Test User',
            'homedir': '/home/test', 'shell': '/bin/bash',
            'user_dn': 'uid=test,%s' % settings.USER_BASE_DN
        }, result)

    def test_create_ldap_user_existing(self):
        result = create_ldap_user(
            'existing', 815, 4711, 'existing test user', '/home/existing',
            '/bin/bash', 'secret'
        )
        self.assertEqual({
            'username': 'existing', 'uid': 815, 'gid': 4711,
            'gecos': 'existing test user', 'homedir': '/home/existing',
            'shell': '/bin/bash',
            'user_dn': u'uid=existing,%s' % settings.USER_BASE_DN
        }, result)

    def test_set_ldap_user_password_existing(self):
        result = set_ldap_user_password('existing', 'newpassword')
        self.assertEqual({
            'username': 'existing', 'password_set': True
        }, result)

    def test_set_ldap_user_password_missing(self):
        result = set_ldap_user_password('missing', 'newpassword')
        self.assertEqual({
            'username': 'missing', 'password_set': False
        }, result)

    def test_add_ldap_user_to_group_existing(self):
        result = add_ldap_user_to_group('existing', 'existing')
        self.assertEqual({
            'username': 'existing', 'groupname': 'existing', 'added': True
        }, result)

    def test_add_ldap_user_to_group_new_user(self):
        create_ldap_group('test', 5000, 'test group')
        result = add_ldap_user_to_group('existing', 'test')
        self.assertEqual({
            'username': 'existing', 'groupname': 'test', 'added': True
        }, result)

    def test_add_ldap_user_to_group_no_group(self):
        result = add_ldap_user_to_group('existing', 'test')
        self.assertEqual({
            'username': 'existing', 'groupname': 'test', 'added': False
        }, result)

    def test_add_ldap_user_to_group_no_user(self):
        with self.assertRaises(LdapUser.DoesNotExist):
            add_ldap_user_to_group('test', 'existing')

    def test_remove_ldap_user_from_group_existing(self):
        result = remove_ldap_user_from_group('existing', 'existing')
        self.assertEqual({
            'username': 'existing', 'groupname': 'existing', 'removed': True
        }, result)
        self.assertNotIn('memberUid', self.ldap_server.get(
            'cn=existing,' + settings.GROUP_BASE_DN))

    def test_remove_ldap_user_from_group_not_in_group(self):
        create_ldap_group('test', 5000, 'test group')
        result = remove_ldap_user_from_group('existing', 'test')
        self.assertEqual({
            'username': 'existing', 'groupname': 'test', 'removed': False
        }, result)

    def test_remove_ldap_user_from_group_no_group(self):
        result = remove_ldap_user_from_group('existing', 'test')
        self.assertEqual({
            'username': 'existing', 'groupname': 'test', 'removed': False
        }, result)

    def test_remove_ldap_user_from_group_no_user(self):
        result = remove_ldap_user_from_group('test', 'existing')
        self.assertEqual({
            'username': 'test', 'groupname': 'existing', 'removed': False
        }, result)

    def test_delete_ldap_user_existing(self):
        result = delete_ldap_user('existing')
        self.assertEqual({'username': 'existing', 'deleted': True}, result)
        with self.assertRaises(KeyError):
            self.ldap_server.get('uid=existing,' + settings.USER_BASE_DN)
        self.assertNotIn('memberUid', self.ldap_server.get(
            'cn=existing,' + settings.GROUP_BASE_DN))

    def test_delete_ldap_user_missing(self):
        result = delete_ldap_user('missing')
        self.assertEqual({'username': 'missing', 'deleted': False}, result)

    def test_delete_ldap_user_no_group(self):
        self.ldap_server.get('uid=existing,' + settings.USER_BASE_DN)[
            'gidNumber'] = '5000'
        result = delete_ldap_user('existing')
        self.assertEqual({'username': 'existing', 'deleted': True}, result)
        with self.assertRaises(KeyError):
            self.ldap_server.get('uid=existing,' + settings.USER_BASE_DN)

    def test_delete_ldap_user_chained_exsting(self):
        result = delete_ldap_user_chained({'username': 'existing'})
        self.assertEqual({'username': 'existing', 'deleted': True}, result)
        with self.assertRaises(KeyError):
            self.ldap_server.get('uid=existing,' + settings.USER_BASE_DN)
        group_object = self.ldap_server.get('cn=existing,' + settings.GROUP_BASE_DN)
        self.assertNotIn('memberUid', group_object)

    def test_delete_ldap_group_if_empty_nonempty(self):
        result = delete_ldap_group_if_empty('existing')
        self.assertEqual({'groupname': 'existing', 'deleted': False}, result)
        ldap_object = self.ldap_server.get('cn=existing,' + settings.GROUP_BASE_DN)
        self.assertIsNotNone(ldap_object)

    def test_delete_ldap_group_if_empty_missing(self):
        result = delete_ldap_group_if_empty('missing')
        self.assertEqual({'groupname': 'missing', 'deleted': False}, result)

    def test_delete_ldap_group_if_empty_empty(self):
        self.ldap_server.add({'cn=emptygroup,' + settings.GROUP_BASE_DN: {
            'objectClass': ['posixGroup'],
            'gidNumber': ['4712'],
            'cn': ['existing'],
            'description': ['existing test group'],
        }})
        result = delete_ldap_group_if_empty('emptygroup')
        self.assertEqual({'groupname': 'emptygroup', 'deleted': True}, result)
        with self.assertRaises(KeyError):
            self.ldap_server.get('cn=emptygroup,' + settings.GROUP_BASE_DN)

    def test_delete_ldap_group_existing(self):
        result = delete_ldap_group('existing')
        self.assertEqual({'groupname': 'existing', 'deleted': True}, result)
        with self.assertRaises(KeyError):
            self.ldap_server.get('cn=existing,' + settings.GROUP_BASE_DN)

    def test_delete_ldap_group_missing(self):
        result = delete_ldap_group('missing')
        self.assertEqual({'groupname': 'missing', 'deleted': False}, result)
