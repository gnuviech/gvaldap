#!/bin/sh

set -e

. /home/gvaldap/gvaldap-venv/bin/activate
cd /srv/gvaldap/gvaldap
celery -A ldaptasks worker -Q ldap -l info
