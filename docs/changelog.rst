Changelog
=========

* :release:`0.7.0 <2020-04-06>`
* :support:`-` update Vagrant setup to Debian Buster and Python 3

* :release:`0.6.0 <2020-03-03>`
* :support:`-` add Python 3 support
* :support:`-` upgrade to Django 2.2.10
* :support:`-` use Pipenv for dependency management
* :feature:`-` properly handle unavailable LDAP server

* :release:`0.5.2 <2015-01-29>`
* :bug:`-` fix minor log message issue

* :release:`0.5.1 <2015-01-29>`
* :bug:`-` reverted Reject handling in ldaptasks

* :release:`0.5.0 <2015-01-29>`
* :feature:`-` improved logging of ldaptasks
* :support:`-` update bpython to 0.13.2, add explicit dependency on requests
  2.5.1
* :support:`-` add explicit dependency for Pygments used by Sphinxdoc
* :support:`-` remove pyyaml dependency as json is used for message
  serialization
* :support:`-` update to Django version 1.7.4

* :release:`0.4.0 <2015-01-24>`
* :feature:`-` add new task :py:func:`ldaptasks.tasks.set_ldap_user_password`

* :release:`0.3.0 <2015-01-19>`
* :support:`-` move tasks from osusers to ldaptasks.tasks

* :release:`0.2.0 <2014-12-29>`
* :feature:`-` add task :py:func:`osusers.tasks.delete_ldap_group`
* :support:`-` use celery routers from gvacommon

* :release:`0.1.3 <2014-12-26>`
* :support:`-` add celery routing for file server tasks

* :release:`0.1.2 <2014-12-17>`
* :support:`-` update Django and other dependencies

* :release:`0.1.1 <2014-05-31>`
* :support:`-` improve production settings
  - no custom cache and database
  - get `ALLOWED_HOSTS` and `SERVER_EMAIL` from environment variables

* :release:`0.1 <2014-05-31>`
* :feature:`-` intial support for creating LDAP users and groups
