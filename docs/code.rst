==================
Code documentation
==================

.. index:: Django

gvaldap is implemented as `Django`_ project and provides some `Celery`_ tasks.

.. _Django: https://www.djangoproject.com/
.. _Celery: http://www.celeryproject.org/


The project module :py:mod:`gvaldap`
====================================

.. automodule:: gvaldap


:py:mod:`urls <gvaldap.urls>`
-----------------------------

.. automodule:: gvaldap.urls


:py:mod:`wsgi <gvaldap.wsgi>`
-----------------------------

.. automodule:: gvaldap.wsgi
   :members:


:py:mod:`settings <gvaldap.settings>`
-------------------------------------

.. automodule:: gvaldap.settings


:py:mod:`ldapentities` app
==========================

.. automodule:: ldapentities


:py:mod:`admin <ldapenties.admin>`
----------------------------------

.. automodule:: ldapentities.admin
   :members:


:py:mod:`models <ldapenties.models>`
------------------------------------

.. automodule:: ldapentities.models
   :members:


:py:mod:`ldaptasks` app
=======================

.. automodule:: ldaptasks


:py:mod:`celery <ldaptasks.celery>`
-----------------------------------

.. automodule:: ldaptasks.celery
   :members:


:py:mod:`tasks <ldaptasks.tasks>`
---------------------------------

.. automodule:: ldaptasks.tasks
   :members:
   :undoc-members:
