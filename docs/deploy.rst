Deploy
========

The production deployment for gvaldap is performed using saltstack and consists
of the following steps:

* installation of native dependencies
* setup of a virtualenv
* installation of gvaldap production dependencies inside the virtualenv
* setup of celery worker under control of systemd
