ARG DEBIAN_RELEASE=buster
FROM debian:$DEBIAN_RELEASE
LABEL maintainer="Jan Dittberner <jan@dittberner.info>"

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    build-essential \
    dumb-init \
    gettext \
    git \
    python3-dev \
    python3-pip \
    python3-setuptools \
    python3-virtualenv \
    python3-wheel \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*.*

RUN python3 -m pip install --prefix=/usr/local pipenv

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libldap2-dev \
    libsasl2-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*.*

ARG GVAGID=2000
ARG GVAUID=2000

ARG GVAAPP=gvaldap

WORKDIR /srv/$GVAAPP

COPY Pipfile Pipfile.lock /srv/$GVAAPP/

RUN addgroup --gid $GVAGID $GVAAPP ; \
    adduser --home /home/$GVAAPP --shell /bin/bash --uid $GVAUID --gid $GVAGID --disabled-password --gecos "User for gnuviechadmin component $GVAAPP" $GVAAPP

USER $GVAAPP
RUN python3 -m virtualenv --python=python3 /home/$GVAAPP/$GVAAPP-venv ; \
    /home/$GVAAPP/$GVAAPP-venv/bin/python3 -m pip install -U pip ; \
    VIRTUAL_ENV=/home/$GVAAPP/$GVAAPP-venv pipenv install --deploy --ignore-pipfile --dev

VOLUME /srv/$GVAAPP

COPY gvaldap.sh /srv/

ENTRYPOINT ["dumb-init", "/srv/gvaldap.sh"]
